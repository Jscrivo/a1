﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM_Machine


{
    class InvalidTransaction: Exception
    {
        public InvalidTransaction(string message): base(message) { }

        //Class used when an invalid transaction has occured
    }

    class InvalidValue: Exception
    {
        public InvalidValue(string message): base(message) { }

        //Class used when an invalid value is used
    }

    class Account
    {
        //Defines a bank account its associated attributes and operations.



        private const int ACCOUNT_TYPE_CHECQUING = 1, ACCOUNT_TYPE_SAVINGS = 2;//Constant representing a savings account type

        private readonly int accountNumber;
        private readonly string accountHolder;
        private float balance, annualIntrRate;

        public Account(int accNum, string holderName, float intrRate)
        {
            //Initialize the account object

            accountNumber = accNum;
            accountHolder = holderName;
            balance = 0.0F;
            annualIntrRate = intrRate;


        }

        public int GetAccountNumber()
        {
            //Return the account number
            return accountNumber;
        }

        public string GetAcctHolderName()
        {
            //Return the account holder name
            return accountHolder;
        }
        
        public float GetBalance()
        {
            //Return the account balance
            return balance;
        }

        public float GetAnnualIntrRate()
        {
            //Return the Annual interest rate
            return annualIntrRate;
        }

        public void SetAnnualIntrRate(float intrRatePercentage)
        {
            //Sets the Annual interest rate
            annualIntrRate = intrRatePercentage/100;
        }

        public float GetMonthlyIntrRate()
        {
            //Returns the monthly interest rate
            return annualIntrRate / 12;
        }

        public float Deposit(float amount)
        {
            //Deposits money into the account, checks if amount is valid

            /* 
             Arguments:
                amount: float
 
             Return Type: float
             */
            if (amount < 0)
            {
                throw new InvalidTransaction("Invalid amount provided. Cannot deposit a negative amount.");
            }

            balance += amount;

            return balance;
        }

        public float Withdraw(float amount)
        {
            //Withdraws money from the account, checks if the amount is valid

            /* 
             Arguments:
                amount: float

             Return Type: float 
             */

            if (amount < 0)
            {
                throw new InvalidTransaction("Invalid amount provided. Cannot deposit a negative amount.");
            }

            if (amount > balance)
            {
                throw new InvalidTransaction("Insufficient funds. Cannot withdraw the providefd amount.");
            }

            balance -= amount;

            return balance;
        }
    }
}
