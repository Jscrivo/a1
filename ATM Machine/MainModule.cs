﻿using System;

namespace ATM_Machine
{
    class MainModule
    {
        static void Main(string[] args)
        {
            //creates and runs the application object
            ATMApplication app = new ATMApplication();

            app.Run();
        }
    }
}