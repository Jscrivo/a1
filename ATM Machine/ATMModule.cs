﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM_Machine
{
    class ATM
    {
        private Bank bank;

        //The Atm class represents an ATM machine.

        //Main Menu
        private const int SELECT_ACCOUNT_OPTION = 1, CREATE_ACCOUNT_OPTION = 2, EXIT_ATM_APPLICATION_OPTION = 3;

        //Account Menu
        private const int CHECK_BALANCE_OPTION = 1, WITHDRAW_OPTION = 2, DEPOSIT_OPTION = 3, EXIT_ACCOUNT_OPTION = 4;

        public ATM(Bank bank)
        {
            //This intializes the ATM machine
            this.bank = bank;
        }

        public void Start()
        { //Starts the ATM machine
            while (true)
            {

                int selectedOption = ShowMainMenu();

                if (selectedOption == SELECT_ACCOUNT_OPTION)
                {
                    Account acct = SelectAccount();
                    if (acct != null)
                    {
                        ManageAccount(acct);
                    }

                }
                else if (selectedOption == CREATE_ACCOUNT_OPTION)
                {
                    OnCreateAccount();
                }
                else if (selectedOption == EXIT_ATM_APPLICATION_OPTION)
                {
                    return;
                }
                else
                {
                    Console.WriteLine("Please enter a valid menu option\n");
                }

            }
        }

        private int ShowMainMenu()
        { //Displays the main menu and reads input
            while (true)
            {
                try
                {
                    Console.WriteLine("\nMain Menu\n\n1: Select Account\n2: Create Account\n3: Exit\n\nEnter a choice: ");
                    return int.Parse(Console.ReadLine());
                } catch (FormatException)
                {
                    Console.WriteLine("Please enter a valid menu option\n");
                }
            }
        }

        private int ShowAccountMenu()
        { //Displays the account menu and reads input
            while (true)
            {
                try
                {
                    Console.WriteLine("\nAccount Menu\n\n1: Check Balance\n2: Withdraw\n3: Deposit\n4: Exit\n\nEnter a choice: ");
                    return int.Parse(Console.ReadLine());
                } catch (FormatException)
                {
                    Console.WriteLine("Please enter a valid menu option.\n");
                }
            }
        }

        private void OnCreateAccount()
        { //Event run when an account needs to be created, collects information about the account from the user
            while (true)
            {
                try
                {
                    //get the name of the account holder from the user
                    string clientName = PromptForClientName();

                    //get initial deposit amount
                    float initDepositAmount = PromptForDepositAmount();

                    //get annual interest rate
                    float annIntrRate = PromptForAnnualIntrRate();

                    //open new account
                    Account newAccount = bank.OpenAccount(clientName, annIntrRate);

                    newAccount.Deposit(initDepositAmount);

                    return;


                } catch(InvalidValue e)
                {
                    Console.WriteLine(e);
                } catch(OperationCancel e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private Account SelectAccount()
        { //Prompts the user for an account number
            while (true)
            {
                try
                {
                    Console.WriteLine("Please enter your account ID or press [ENTER] to cancel: ");
                    string acctNoInput = Console.ReadLine();

                    if (acctNoInput.Length == 0)
                    {
                        return null;
                    }

                    int acctNo = int.Parse(acctNoInput);

                    Account acct = bank.FindAccount(acctNo);
                    if (acct != null)
                    {
                        return acct;
                    } else
                    {
                        Console.WriteLine("The account was not found. Please select another account.");
                    }


                } catch (FormatException)
                {
                    Console.WriteLine("Please enter a valid account number (ex: 100)\n");
                }
            }
        }

        private void ManageAccount(Account account)
        { //Manage the account by allowing the user execute various operations
            while (true)
            {
                int selAcctMenuOpt = ShowAccountMenu();

                if (selAcctMenuOpt == CHECK_BALANCE_OPTION)
                {
                    OnCheckBalance(account);
                } else if (selAcctMenuOpt == WITHDRAW_OPTION)
                {
                    OnWithdraw(account);
                } else if (selAcctMenuOpt == DEPOSIT_OPTION)
                {
                    OnDeposit(account);
                } else if (selAcctMenuOpt == EXIT_ACCOUNT_OPTION)
                {
                    return;
                } else
                {
                    Console.WriteLine("Please enter a valid menu option");
                }
            }
        }

        private string PromptForClientName()
        { //Prompt the user for the name of the client
            Console.WriteLine("Please enter the client name or press [ENTER] to cancel:");
            string clientName = Console.ReadLine();

            if (clientName.Length == 0)
            {
                throw new OperationCancel("The user has selected to cancel the current opertation");

            }

            return clientName;
        }

        private float PromptForDepositAmount()
        { //Prompt the user to enter an account balance, performs basic error detection 
            while (true)
            {
                try
                {
                    Console.WriteLine("PLease enter your initial account balance: ");
                    float initAmount = float.Parse(Console.ReadLine());

                    if (initAmount >= 0)
                    {
                        return initAmount;
                    } else
                    {
                        Console.WriteLine("Cannot create an account with a negative initial balance. Please enter a valid amount");
                    }
                } catch (FormatException e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private float PromptForAnnualIntrRate()
        { //Prompt the user to enter the annual interest rate for the account
            while (true)
            {
                try
                {
                    Console.WriteLine("Please enter the interest rate for this account: ");
                    float intrRate = float.Parse(Console.ReadLine());

                    if (intrRate >= 0)
                    {
                        return intrRate;
                    } else
                    {
                        Console.WriteLine("Cannot create an account with a negative interest rate.");
                    }

                } catch (FormatException e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private void OnCheckBalance(Account account)
        { //Prints the balance of the account
            Console.WriteLine("The balance is " + account.GetBalance() + "\n");
        }

        private void OnDeposit(Account account)
        { //Prompts the user for an amount and performs the deposit
            while (true)
            {
                try
                {
                    Console.WriteLine("Please enter an amount to deposit or type [ENTER] to exit: ");
                    string inputAmount = Console.ReadLine();
                    if (inputAmount.Length > 0)
                    {
                        account.Deposit(float.Parse(inputAmount));
                    }

                    return;

                } catch (FormatException)
                {
                    Console.WriteLine("Invalid entry. Please enter a number for your amount.\n");
                } catch (InvalidTransaction e)
                {
                    Console.WriteLine(e + "\n");
                }
            }
        }

        private void OnWithdraw(Account account)
        { //Prompts the user for an amount and performs the withdrawal
            while (true)
            {
                try
                {
                    Console.WriteLine("Please enter an amount to withdraw or type [ENTER] to exit: ");
                    string inputAmount = Console.ReadLine();
                    if (inputAmount.Length > 0)
                    {
                        account.Withdraw(float.Parse(inputAmount));
                    }

                    return;

                } catch (FormatException)
                {
                    Console.WriteLine("Invalid entry. Please enter a number for your amount.\n");
                } catch (InvalidTransaction e)
                {
                    Console.WriteLine(e + "\n");
                }
            }
        }

    }
}
