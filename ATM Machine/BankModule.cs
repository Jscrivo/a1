﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM_Machine
{
    class OperationCancel: Exception
    {
        public OperationCancel(string message): base(message) { }
        //Called when the user cancels an operation
    }

    class Bank
    {
        /// <summary>
        /// Represents the bank with a list of accounts
        /// </summary>

        private const int DEFAULT_ACCT_NO_START = 100;
        private List<Account> accountList;

       public Bank()
        {
            //Intializes a bank object
            accountList = new List<Account>();
        }

       public Account FindAccount(int acctNo)
        {
            //Finds the account with the matching number in the list, returns null if not found

            /*
             Arguments:
                acctNo: int
             
             Return Type: Account
             */
            foreach (Account acct in accountList)
            {
                if (acct.GetAccountNumber() == acctNo)
                {
                    return acct;
                }
            }

            return null;
        }

        private int DetermineAccountNumber()
        {
            /*
             Return type: int
            */

            //Generates a valid account number

            string acctNoInput;
            int acctNo;

            while (true)
            {
                try
                {
                    //ask user for input
                    Console.WriteLine("Please enter the account number [100 - 1000] or press [ENTER] to cancel:");
                    acctNoInput = Console.ReadLine();

                    //check if user wants to cancel
                    if (acctNoInput.Length == 0)
                    {
                        throw new OperationCancel("User has selected to terminate the program after invalid input");
                    }

                    //convert string to int
                    acctNo = int.Parse(acctNoInput);
                    //check if input is valid
                    if (acctNo < 100 || acctNo > 1000)
                    {
                        throw new InvalidValue("The account number you have entered is not valid. Please enter a valid account number");
                    }

                    //check if account number already exists
                    foreach(Account acct in accountList)
                    {
                        if (acct.GetAccountNumber() == acctNo)
                        {
                            throw new InvalidValue("The account number you have entered already exists. Please use a different account number");
                        }
                    }

                    //return generated number
                    return acctNo;


                } catch(FormatException e)
                {
                    Console.WriteLine(e);
                }
                catch (InvalidValue e)
                {
                    Console.WriteLine(e);
                }
            }
        }

         public Account OpenAccount(string clientName, float annIntrRate)
        {
            //Opens a new account
            /*
            Arguments:
            clientName: string

            Return type: Account
            */

            int acctNo = DetermineAccountNumber();

            Account newAccount = new Account(acctNo, clientName, annIntrRate);

            accountList.Add(newAccount);

            return newAccount;
        }
    }
}
