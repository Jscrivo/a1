﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM_Machine
{
    class ATMApplication
    { //Ensures the program does not crash
        public void Run()
        {
            try
            {
                Bank bank = new Bank();

                ATM atm = new ATM(bank);

                atm.Start();
            } catch (Exception e)
            {
                Console.WriteLine("An error occurred with the following message: " + e);

            }
        }
    }
}
